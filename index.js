let http=require("http");
const PORT=3000;

http.createServer((req,res)=>{
		if(req.url=="/login"){
			res.writeHead(200,{"Content-Type": "text/html"});
			res.write("Welcome to the login page");
			res.end();
		}
		else{
			res.writeHead(404, {"Content-Type": "text/plain"});
			res.end("I'm sorry the page you are looking for cannot be found");
		}
}).listen(PORT);
console.log("Server is successfully running")